import "lib-flexible";
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store/store'
import Navigation from 'vue-navigation'
import {
    get,
    post
} from './lib/axios'

import * as qiniu from 'qiniu-js'
Vue.use(qiniu)


import global from './component/global'




import _ from 'lodash'
import Storage from 'vue-ls'
import layer from 'vue-layer-mobile'
// import './asset/layermobile/layer.css'
import Swiper from 'swiper'
// mint UI 
import Mint from 'mint-ui';
Vue.use(Mint);
// mint UI 结束
// ydui
import YDUI from 'vue-ydui'; /* 相当于import YDUI from 'vue-ydui/ydui.rem.js' */
// import 'vue-ydui/dist/ydui.px.css';
Vue.use(YDUI);
// ydui
// scroller
import VueScroller from 'vue-scroller'
Vue.use(VueScroller)
// scroller
//VConsole
import VConsole from './config/vconsole';
//VConsole


Vue.config.productionTip = false

Vue.use(Navigation, {
    router
})
// 公共配置
Vue.prototype.global = global;
// axios
Vue.prototype.$get = get
Vue.prototype.$post = post

// cookie
// Vue.prototype.getCookie = getCookie
// Vue.prototype.setCookie = setCookie
// Vue.prototype.delCookie = delCookie

// layer-mobile
Vue.use(layer)

// lodash
Vue.prototype._ = _

// vue-ls (Vue plugin for work with local storage, session storage and memory storage from Vue context)
let options = {
    namespace: 'vuejs__', // key prefix
    name: 'ls', // name variable Vue.[ls] or this.[$ls],
    storage: 'local' // storage name session, local, memory
}
Vue.use(Storage, options)

let vm = new Vue({
    el: '#app',
    router,
    store,
    components: {
        App
    },
    template: '<App/>',
    watch: {
        $route() {
            document.title = this.$route.meta.title + global.WEB_NAME
        }
    }
})
Vue.use({
    vm
})
