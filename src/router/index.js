import Vue from "vue";
import VueRouter from "vue-router";
import {
    post
}
from "../lib/axios";
import global from '../component/global'
Vue.use(VueRouter);
localStorage.getItem('token')
if (process.env.NODE_ENV === "development") {
    localStorage.setItem("token", "948fb95a9026f56e5466aef98338b494");
}

const Routers = [{
        path: "/",
        meta: {
            title: "优惠券"
        },
        component: resolve => require(["@/page/coupon/coupon"], resolve)
    },
    {
        path: "/home",
        meta: {
            title: "优惠券",
        },
        component: resolve => require(["@/page/home/home"], resolve),
    },
    {
        path: "/wxCont",
        meta: {
            title: "公众号任务",
        },
        component: resolve => require(["@/page/home/wxCont"], resolve),
    },
    {
        path: "/getPassword",
        meta: {
            title: "获取口令",
        },
        component: resolve => require(["@/page/home/getPassword"], resolve),
    },
    {
        path: "/enter",
        meta: {
            title: "输入口令",
        },
        component: resolve => require(["@/page/home/enter"], resolve),
    },
    {
        path: "/goods",
        meta: {
            title: "商品详情"
        },
        component: resolve => require(["@/page/home/goods"], resolve)
    },
    {
        path: "/couponList",
        meta: {
            title: "优惠券"
        },
        component: resolve => require(["@/page/coupon/couponList"], resolve)
    },
    {
        path: "/coupon",
        meta: {
            title: "优惠券"
        },
        component: resolve => require(["@/page/coupon/coupon"], resolve)
    },
    {
        path: "/couponSearch",
        meta: {
            title: "商品搜索",
        },
        component: resolve => require(["@/page/coupon/couponSearch"], resolve)
    },
    {
        path: "/couponCont",
        meta: {
            title: "优惠券详情"
        },
        component: resolve => require(["@/page/coupon/couponCont"], resolve)
    },
    {
        path: "/couponShare",
        meta: {
            title: "优惠券分享"
        },
        component: resolve => require(["@/page/coupon/couponShare"], resolve)
    },
    {
        path: "/couponPay",
        meta: {
            title: "确认订单"
        },
        component: resolve => require(["@/page/coupon/couponPay"], resolve)
    },
    {
        path: "/news",
        meta: {
            title: "消息"
        },
        component: resolve => require(["@/page/news/news"], resolve)
    },
    {
        path: "/mine",
        meta: {
            title: "个人中心"
        },
        component: resolve => require(["@/page/mine/mine"], resolve)
    },
    {
        path: "/authentication",
        meta: {
            title: "实名认证"
        },
        component: resolve => require(["@/page/mine/authentication"], resolve)
    },
    {
        path: "/member",
        meta: {
            title: "成为会员"
        },
        component: resolve => require(["@/page/mine/member"], resolve)
    },
    {
        path: "/userInfo",
        meta: {
            title: "个人信息"
        },
        component: resolve => require(["@/page/mine/userInfo"], resolve)
    },

    {
        path: "/money",
        meta: {
            title: "收入明细"
        },
        component: resolve => require(["@/page/money/money"], resolve),
        children: [{
                path: "income",
                meta: {
                    title: "收入"
                },
                component: resolve =>
                    require(["@/page/money/income"], resolve)
            },
            {
                path: "expenditure",
                meta: {
                    title: "支出"
                },
                component: resolve =>
                    require(["@/page/money/expenditure"], resolve)
            },
        ]
    },
    {
        path: "/Invitation",
        meta: {
            title: "我的邀请"
        },
        component: resolve => require(["@/page/Invitation/Invitation"], resolve),
        children: [{
                path: "InvitationVip",
                meta: {
                    title: "邀请会员好友"
                },
                component: resolve =>
                    require(["@/page/Invitation/InvitationVip"], resolve)
            },
            {
                path: "InvitationOrdinary",
                meta: {
                    title: "邀请普通好友"
                },
                component: resolve =>
                    require(["@/page/Invitation/InvitationOrdinary"], resolve)
            },
        ]
    },
    {
        path: "/addAccount",
        meta: {
            title: "添加账户"
        },
        component: resolve => require(["@/page/mine/addAccount"], resolve)
    },
    {
        path: "/friend",
        meta: {
            title: "邀请好友"
        },
        component: resolve => require(["@/page/mine/friend"], resolve)
    },
    {
        path: "/drawMoney",
        meta: {
            title: "提现"
        },
        component: resolve => require(["@/page/mine/drawMoney"], resolve)
    },
    {
        path: "/order",
        meta: {
            title: "订单中心"
        },
        component: resolve => require(["@/page/order/order"], resolve),
        children: [{
                path: "orderAll",
                meta: {
                    title: "全部"
                },
                component: resolve =>
                    require(["@/page/order/orderAll"], resolve)
            },
            {
                path: "orderUse",
                meta: {
                    title: "待付款"
                },
                component: resolve =>
                    require(["@/page/order/orderUse"], resolve)
            },
            {
                path: "orderFinish",
                meta: {
                    title: "已完成"
                },
                component: resolve =>
                    require(["@/page/order/orderFinish"], resolve)
            }
        ]
    },
    {
        path: "/orderCont",
        meta: {
            title: "订单详情"
        },
        component: resolve => require(["@/page/order/orderCont"], resolve)
    }
];

const RouterConfig = {
    //   mode: 'history',
    base: '',
    mode: "hash",
    routes: Routers
};
const router = new VueRouter(RouterConfig);
router.beforeEach((to, from, next) => {
    // return;
    let href = window.location.href;
    console.error('当前路径', href);
    let token = localStorage.getItem("token") // 获取本地存储token
    if (!!token) {
        localStorage.removeItem('goBackUrl')
        if (href.includes("coupon_id=") && href.includes("&from")) {
            let rightIndex = href.indexOf("&coupon_id=");
            let newCoupon_id = href.substring(rightIndex + 11, href.indexOf("&from="));
            localStorage.setItem('coupon_id', newCoupon_id);
            window.location.href = 'http://wangzhuan.3todo.cn/#/'
            return;
        }
        if (href.includes("type=wx&")) {
            window.location.href = 'http://wangzhuan.3todo.cn/#/'
            return;
        }
        if (href.includes("type=coupon&coupon_id")) {
            let leftIndex = href.indexOf("coupon_id=");
            let rightIndex = href.indexOf("&end");
            let newId = href.substring(leftIndex + 10, rightIndex);
            window.location.href = 'http://wangzhuan.3todo.cn/#/' + 'coupon?couponId=' + newId + '&shareType=coupon'
            return;
        }
        if (from.path == '/home') {
            next();
            return;
        }
        next();
        return;
    }

    if (href.includes("share_id=")) {
        let leftIndex = href.indexOf("share_id=");
        let rightIndex = href.indexOf("&from=");
        if (href.includes("coupon_id=")) {
            rightIndex = href.indexOf("&coupon_id=");
            let newCoupon_id = href.substring(rightIndex + 11, href.indexOf("&from="));
            localStorage.setItem('coupon_id', newCoupon_id);
        }
        let newdata = href.substring(leftIndex + 9, rightIndex);
        localStorage.setItem('share_id', newdata);
        // setTimeout(() => {
        window.location.href = "https://open.weixin.qq.com/connect/oauth2/authorize?appid=" + global.APPID + "&redirect_uri=" + global.WEB_URL + "&response_type=code&scope=snsapi_userinfo#wechat_redirect";
        // }, 10000);
        return;
    }

    if (href.includes("?code")) {
        let urlLeftIndex = href.indexOf("code=") + 5; //获取域名结束的位置
        let urlRightIndex = href.indexOf("&state"); //获取域名结束的位置
        let code = href.substring(urlLeftIndex, urlRightIndex); //url右侧部分
        let data = {
            code: code,
        };
        if (!!localStorage.getItem('share_id')) {
            data.pid = localStorage.getItem('share_id')
        }
        post("/index/login", data)
            .then(res => {
                if (res.code === 20000) {
                    let backUrl = localStorage.getItem('goBackUrl')
                    console.log(backUrl);
                    localStorage.setItem("token", res.data.token);
                    localStorage.removeItem('share_id')

                    // setTimeout(() => {
                    if (!backUrl) {
                        window.location.href = global.WEB_URL
                        return;
                    }
                    window.location.href = backUrl
                    // }, 5000);

                }
            })
            .catch(err => {});
        return;
    } else {
        if (!token) {
            localStorage.setItem("goBackUrl", window.location.href);
        }
        console.log(4);
        // setTimeout(() => {
        window.location.href = "https://open.weixin.qq.com/connect/oauth2/authorize?appid=" + global.APPID + "&redirect_uri=" + global.WEB_URL + "&response_type=code&scope=snsapi_userinfo#wechat_redirect";
        // }, 5000);
        return;
    }

});
router.afterEach(() => {
    window.scrollTo(0, 0);
});

export default router;
