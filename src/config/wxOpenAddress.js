//微信支付方法（点击按键调用）
let wx = require('weixin-js-sdk');
/*
微信支付方法
获取微信加签信息
@param{data}:获取的微信加签
@param{cb}:成功回调
*/
let wxOpenAddress = (data, shopdata) => {
    console.log(shopdata);
    wx.config({
        debug: false, // 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。
        appId: data.appId, // 必填，公众号的唯一标识
        timestamp: data.timestamp, // 必填，生成签名的时间戳
        nonceStr: data.nonceStr, // 必填，生成签名的随机串
        signature: data.signature, // 必填，签名，见附录1
        jsApiList: data.jsApiList // 必填，需要使用的JS接口列表，所有JS接口列表见附录2
    });
    wx.ready(function () {
        wx.openLocation({
            latitude: parseFloat(shopdata.lat), // 纬度，浮点数，范围为90 ~ -90
            longitude: parseFloat(shopdata.lon), // 经度，浮点数，范围为180 ~ -180。
            name: shopdata.shop_name, // 位置名
            address: shopdata.address, // 地址详情说明
            scale: 14, // 地图缩放级别,整形值,范围从1~28。默认为最大
            infoUrl: '' // 在查看位置界面底部显示的超链接,可点击跳转
        });

    });
    wx.error(function (res) {});
}
export default wxOpenAddress;
