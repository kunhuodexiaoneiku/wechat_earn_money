/* eslint-disable quotes */
/* eslint-disable semi */
/* eslint-disable indent */
let wx = require("weixin-js-sdk");
/*
微信支付方法
获取微信加签信息
@param{data}:获取的微信加签
@param{cb}:成功回调
*/
let wxShare = (data, content, WXShareSuccess) => {
    let title = content.nickname + '邀请你加入来逛优！！！';
    let descr = '加入来逛优·一起赚钱吧';
    let icon = content.head_img;
    let url = location.href.split('#')[0] + '?share_id=' + localStorage.getItem('token');
    console.log(url);
    wx.config({
        debug: false,
        appId: data.appId,
        timestamp: data.timestamp,
        nonceStr: data.nonceStr,
        signature: data.signature,
        jsApiList: ['onMenuShareAppMessage', 'onMenuShareTimeline', 'onMenuShareQQ', 'onMenuShareQZone']
    });
    wx.ready(function () {
        //分享给朋友
        wx.onMenuShareAppMessage({
            title: title, // 分享标题
            desc: descr, // 分享描述
            link: url, // 分享链接，该链接域名或路径必须与当前页面对应的公众号JS安全域名一致
            imgUrl: icon, // 分享图标
            success: function (res) {
                WXShareSuccess();
            }
        });
        //分享到朋友圈
        wx.onMenuShareTimeline({
            title: title, // 分享标题
            link: url, // 分享链接，该链接域名或路径必须与当前页面对应的公众号JS安全域名一致
            imgUrl: icon, // 分享图标
            success: function (res) {
                WXShareSuccess();
            }
        });
        //QQ
        wx.onMenuShareQQ({
            title: title, // 分享标题
            desc: descr, // 分享描述
            link: url, // 分享链接
            imgUrl: icon, // 分享图标
            success: function () {
                WXShareSuccess();
                // 用户确认分享后执行的回调函数
            },
            cancel: function () {
                // 用户取消分享后执行的回调函数
            }
        });
        //qq空兼
        wx.onMenuShareQZone({
            title: title, // 分享标题
            desc: descr, // 分享描述
            link: url, // 分享链接
            imgUrl: icon, // 分享图标
            success: function () {
                WXShareSuccess();
                // 用户确认分享后执行的回调函数
            },
            cancel: function () {
                // 用户取消分享后执行的回调函数
            }
        });
    });
    wx.error(function (res) {});
};
export default wxShare;
